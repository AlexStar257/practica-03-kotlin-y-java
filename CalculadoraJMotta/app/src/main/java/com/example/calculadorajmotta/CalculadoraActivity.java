package com.example.calculadorajmotta;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class CalculadoraActivity extends AppCompatActivity {
    private Button btnSumar;
    private Button btnRestar;
    private Button btnMulti;
    private Button btnDividir;
    private Button btnLimpiar;
    private Button btnRegresar;

    private EditText txtNum1;
    private EditText txtNum2;
    private TextView lblTotal;
    private Calculadora calculadora = new Calculadora(0.0, 0.0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        iniciarComponentes();

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSumar();
            }
        });
        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRestar();
            }
        });
        btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMulti();
            }
        });
        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDividir();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
    }

    private void limpiar() {
        txtNum1.setText("");
        txtNum2.setText("");
        lblTotal.setText("");
    }

    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("¿Desea regresar?");
        confirmar.setPositiveButton("Confirmar", (dialogInterface, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialogInterface, which) -> {
        }).show();
    }

    private void iniciarComponentes() {
        btnSumar = findViewById(R.id.btnSumar);
        btnRestar = findViewById(R.id.btnRestar);
        btnMulti = findViewById(R.id.btnMulti);
        btnDividir = findViewById(R.id.btnDividir);

        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        txtNum1 = findViewById(R.id.txtNum1);
        txtNum2 = findViewById(R.id.txtNum2);
        lblTotal = findViewById(R.id.lblTotal);
    }

    private void btnSumar() {
        String num1Str = txtNum1.getText().toString();
        String num2Str = txtNum2.getText().toString();

        if (!num1Str.isEmpty() && !num2Str.isEmpty()) {
            calculadora.setNum1(Double.parseDouble(num1Str));
            calculadora.setNum2(Double.parseDouble(num2Str));
            lblTotal.setText(String.valueOf(calculadora.suma()));
        } else {
            Toast.makeText(this, "Ingrese ambos números.", Toast.LENGTH_SHORT).show();
        }
    }

    private void btnRestar() {
        String num1Str = txtNum1.getText().toString();
        String num2Str = txtNum2.getText().toString();

        if (!num1Str.isEmpty() && !num2Str.isEmpty()) {
            calculadora.setNum1(Double.parseDouble(num1Str));
            calculadora.setNum2(Double.parseDouble(num2Str));
            lblTotal.setText(String.valueOf(calculadora.resta()));
        } else {
            Toast.makeText(this, "Ingrese ambos números.", Toast.LENGTH_SHORT).show();
        }
    }

    private void btnMulti() {
        String num1Str = txtNum1.getText().toString();
        String num2Str = txtNum2.getText().toString();

        if (!num1Str.isEmpty() && !num2Str.isEmpty()) {
            calculadora.setNum1(Double.parseDouble(num1Str));
            calculadora.setNum2(Double.parseDouble(num2Str));
            lblTotal.setText(String.valueOf(calculadora.multi()));
        } else {
            Toast.makeText(this, "Ingrese ambos números.", Toast.LENGTH_SHORT).show();
        }
    }

    private void btnDividir() {
        String num1Str = txtNum1.getText().toString();
        String num2Str = txtNum2.getText().toString();

        if (!num1Str.isEmpty() && !num2Str.isEmpty()) {
            calculadora.setNum1(Double.parseDouble(num1Str));
            calculadora.setNum2(Double.parseDouble(num2Str));
            lblTotal.setText(String.valueOf(calculadora.dividir()));
        } else {
            Toast.makeText(this, "Ingrese ambos números.", Toast.LENGTH_SHORT).show();
        }
    }
}

