package com.example.calculadorajmotta;

public class Calculadora {
    private double num1;
    private double num2;

    public Calculadora(double num1, double num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public double suma() {
        return num1 + num2;
    }

    public double resta() {
        return num1 - num2;
    }

    public double multi() {
        return num1 * num2;
    }

    public double dividir() {
        double total = 0.0;
        if (num2 != 0.0) {
            total = num1 / num2;
        } else {
            throw new IllegalArgumentException("No se puede dividir entre cero.");
        }
        return total;
    }

    public void setNum1(double num1) {
        this.num1 = num1;
    }
    public void setNum2(double num2) {
        this.num2 = num2;
    }
}
