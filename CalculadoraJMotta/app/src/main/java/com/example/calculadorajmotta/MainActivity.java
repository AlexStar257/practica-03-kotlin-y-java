package com.example.calculadorajmotta;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button btnIngresar;
    private Button btnRegresar;
    private EditText txtUsuario;
    private EditText txtContraseña;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
    }

    private void iniciarComponentes() {
        btnRegresar = findViewById(R.id.btnRegresar);
        btnIngresar = findViewById(R.id.btnIngresar);
        txtContraseña = findViewById(R.id.txtContraseña);
        txtUsuario = findViewById(R.id.txtUsuario);
    }

    private void ingresar() {
        String strUsuario = getResources().getString(R.string.usuario);
        String strContra = getResources().getString(R.string.contraseña);

        if (txtUsuario.getText().toString().equals(strUsuario) && txtContraseña.getText().toString().equals(strContra)) {
            Bundle bundle = new Bundle();
            bundle.putString("usuario", strUsuario);

            Intent intent = new Intent(MainActivity.this, CalculadoraActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(),
                    "Usuario o Contraseña no válidos", Toast.LENGTH_LONG).show();
        }
    }
    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("¿Desea regresar?");
        confirmar.setPositiveButton("Confirmar", (dialogInterface, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialogInterface, which) -> { });
        confirmar.show();
    }
}
